#!/usr/bin/env python3


import RPi.GPIO as GPIO
import subprocess
import time 

shutd = True
GPIO.setmode(GPIO.BCM)
GPIO.setup(3, GPIO.IN, pull_up_down=GPIO.PUD_UP)	#set input mode on pin 3 and set filter on 



while shutd:
    GPIO.wait_for_edge(3, GPIO.FALLING)		#detect if voltage get down on pin 3 (i.e. pin 3 is connected to ground via the big switch)
    time.sleep(3)				#wait 3 seconds (to avoid spurious voltage falling)
    if GPIO.input(3) == 0:			#check if the pin is still on ground voltage 
        shutd = False
    

subprocess.call(['shutdown', '-h', 'now'], shell=False)		#shutdown of the rasbian OS via python
