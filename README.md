# Camera Trap 13'Infuz

Little project to take night picture at 13'Infuz. Supposed presence of foxes and cataphiles

## Installation

From a Lite Raspbian

```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install build-essential python3-dev python3-picamera python3-rpi.gpio 
sudo apt-get install git
git clone https://framagit.org/yohann-chemtob/piegephotonocturne/
```

To make it start every time the Pi boot, edit the /etc/rc.local with 
```
sudo nano /etc/rc.local
```
and add : 
```
sudo python /home/pi/piegephotonocturne/antisocial.py  &
sudo python /home/pi/piegephotonocturne/listen-for-shutdown.py &
```
