#!/usr/bin/env python3

__author__ = "Yohann Chemtob"
__license__ = "WTFPL"
__version__ = "0.1"

import RPi.GPIO as GPIO
import time
import picamera 
import os


sensorPin = 13
ledPin = 11

# Set the GPIO (General Purpose Input Outout) PINs up and define that we want to read "sensorPin" that we assigned above
GPIO.setmode(GPIO.BOARD)
GPIO.setup(sensorPin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

# Define the state of the PIR i.e what was it doing previously, and what is it doing now - has it triggered?
prevState = False
currState = False

camera = picamera.PiCamera()
camera.resolution = (3280, 2464)
#camera.iso = 50
camera.color_effects = (128,128)

if not os.path.exists('/home/pi/cameratrap01/'):
    os.makedirs('/home/pi/cameratrap01/')

starttime=time.time()

while True:
    tmptime = time.gmtime()		#get present time

    prevState = currState
    currState = GPIO.input(sensorPin)
    if currState != prevState:

        #______________take picture__________________________
        if currState:
            GPIO.output(ledPin, GPIO.HIGH)		#send current to the relai to turn on the IR led
            time.sleep(0.5)			#wait for led to get stable enough
            tmpnamefile = time.strftime("%Y-%m-%dT%H%M%S.jpg", tmptime)		#create img_name
            camera.capture('/home/pi/cameratrap01/'+tmpnamefile)			#take a picture and store in argument 
            time.sleep(0.5)							#wait for the ccd to complete taking picture (just to be safe)
            GPIO.output(ledPin, GPIO.LOW)						#turn off led

